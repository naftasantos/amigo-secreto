$(document).ready(function() {
  if (QueryString["uuid"]) {
    ShowLoading();
    LoadSorteado(QueryString["uuid"]);
  }
});

function ShowLoading() {
  $("#view-sorteado").css("display", "none");
  $("#view-loading").css("display", "flex");
}

function ShowSorteado() {
  $("#view-sorteado").css("display", "flex");
  $("#view-loading").css("display", "none");
}

function PrintSorteadoData(data) {
  var $person = $("#title-amigo-secreto");
  var $sorteado = $("#title-amigo-secreto-sorteado");

  var person_text = $person.html().replace("{{name}}", data["name"]);
  var sorteado_text = $sorteado.html().replace("{{name}}", data["name_sorteado"]);

  $person.html(person_text);
  $sorteado.html(sorteado_text);
}

function LoadSorteado(uuid) {
  var args = {
    "uuid": uuid
  };

  $.ajax({
    type: "GET",
    url: "service/load-sorteado.php",
    data: args,
    success: function(result) {
      ShowSorteado();
      PrintSorteadoData(JSON.parse(result));
    },
    fail: function(evt) {
      ShowCreate();
    }
  });
}