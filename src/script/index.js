$(document).ready(function() {
    $("#add-participant").click(onClickAddParticipant);
    $("#create").click(onClickCreateAmigoSecreto);
    $("#participant").keydown(onParticipantKeyDown);
});

function onClickAddParticipant(evt) {
    AddParticipant($('#participant'));
}

function onClickCreateAmigoSecreto(evt) {
    var participants = ListParticipants();
    var args = {
        "name": $("#nome-amigo-secreto").val(),
        "participants": participants
    }

    $.ajax({
        type: "POST",
        url: "service/create-amigo-secreto.php",
        data: args,
        success: function(result) {
            obj = JSON.parse(result);
            NavigateTo("show_amigo_secreto.html?id=" + obj["id"]);
        },
        fail: function(evt) {
            ShowCreate();
        }
    });

    ShowLoading();
}

function onRemoveItemClick(evt) {
    var $button = $(evt.currentTarget);
    var $parent = $button.parent();

    $parent.remove();
}

function onParticipantKeyDown(evt) {
    if (evt.keyCode == 13) {
        AddParticipant($('#participant'));
    }
}

function AddParticipant($participant) {
    var $item = $('<li class="mdl-list__item">');
    var $name = $('<span class="mdl-list__item-primary-content">');
    var $button = $('<button class="mdl-button mdl-js-button mdl-button--icon">');
    var $icon = $('<i class="material-icons mdl-list__item-icon">delete</i>');

    $name.html($participant.val());
    $button.append($icon);

    $item.append($name);
    $item.append($button);

    $button.click(onRemoveItemClick);

    $('#participant-list').append($item);

    $participant.val('');
    $participant[0].focus();
}

function ShowCreate() {
    $("#view-create").css("display", "flex");
    $("#view-loading").css("display", "none");
}

function ShowLoading() {
    $("#view-create").css("display", "none");
    $("#view-loading").css("display", "flex");
}

function ListParticipants() {
    var participants = $("#participant-list").find("li");
    var len = participants.length;
    var ret = []

    for (var idx=0; idx < len; idx++) {
        var $participant = $(participants[idx]);
        ret.push($participant.find('span').html());
    }

    return ret;
}