$(document).ready(function() {
    if (QueryString["id"]) {
        ShowLoading();
        LoadAmigoSecreto(QueryString["id"]);
    }
    else {
        PrintAmigoSecreto("não informado <br> :(");
    }
});

function ShowLoading() {
    $("#view-list").css("display", "none");
    $("#view-loading").css("display", "flex");
}

function ShowList() {
    $("#view-list").css("display", "flex");
    $("#view-loading").css("display", "none");
}

function PrintAmigoSecretoData(data) {
    PrintAmigoSecreto(data["name"]);
    PrintParticipants(data["participants"]);
}

function PrintAmigoSecreto(name) {
    var $title = $("#title-amigo-secreto")
    var replaced = $title.html().replace("{{name}}", name);

    $title.html(replaced);
}

function PrintParticipants(participants) {
    var len = participants.length;

    for(var idx = 0; idx < len; idx++) {
        PrintParticipant(participants[idx]);
    }
}

function PrintParticipant(participant) {
    var $participant_list = $("#participant-list");
    var $li = $('<li class="mdl-list__item">');
    var $span = $('<span class="mdl-list__item-primary-content">');
    var $button = $('<a class="mdl-button">');

    $span.html(participant['name']);
    $button.html("Quem tirou?");
    $button.attr("uuid", participant["uuid"]);
    $button.attr("href", ConcatToBase('quem_tirei.html?uuid=' + encodeURI(participant["uuid"])));

    $li.append($span);
    $li.append($button);

    $participant_list.append($li);
}

function LoadAmigoSecreto(id) {
    var args = {
        "id": id
    };

    $.ajax({
        type: "GET",
        url: "service/load-amigo-secreto.php",
        data: args,
        success: function(result) {
            ShowList();
            PrintAmigoSecretoData(JSON.parse(result));
        },
        fail: function(evt) {
            console.log(evt);
        }
    });
}