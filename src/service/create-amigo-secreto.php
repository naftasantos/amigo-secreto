<?php
    $name = $_POST["name"];
    $participants = $_POST["participants"];

    $id = SaveToDatabase($name, $participants);

    $ret = array(
        "id" => $id
    );

    echo(json_encode($ret));

    function SaveToDatabase($name, $participants) {
        include 'config.php';

        $connection = new mysqli($DB_SERVER, $DB_USER, $DB_PASS, $DB_NAME);

        if($connection->connect_error) {
            $ret = array(
                "message" => "Connection failed: " . $connection->connect_error,
            );

            http_response_code(500);

            die(json_encode($ret));
        }

        $sql = "INSERT INTO `amigo-secreto` (nm_amigo_secreto) VALUES('" . $name . "')";

        if ($connection->query($sql) == FALSE) {
            $ret = array(
                "message" => "Error creating amigo secreto: " . $connection->error,
            );

            http_response_code(500);

            die(json_encode($ret));
        }

        $amigo_secreto_id = $connection->insert_id;
        $created_participants = array();

        foreach($participants as $participant) {
            $participant_id = SaveParticipantToDatabase($connection, $amigo_secreto_id, $participant);

            $participant_obj = array(
                "id" => $participant_id,
                "name" => $participant
            );

            array_push($created_participants, $participant_obj);
        }

        DoRaffle($connection, $created_participants);

        return $amigo_secreto_id;
    }

    function SaveParticipantToDatabase($connection, $amigo_secreto_id, $participant) {
        $sql = "INSERT INTO `participante` (nm_participante, uuid, id_amigo_secreto) VALUES('" . $participant . "', UUID(), " . $amigo_secreto_id . ")";

        if ($connection->query($sql) == FALSE) {
            $ret = array(
                "message" => "Error creating participante: " . $connection->error,
            );

            http_response_code(500);

            die(json_encode($ret));
        }

        return $connection->insert_id;
    }

    function DoRaffle($connection, $participants) {
        ShuffleArray($participants);
        $len = count($participants);

        for($idx = 0; $idx < $len; $idx++) {
            $next_participant_idx = ($idx + 1) % $len;
            
            $participant = $participants[$idx];
            $next_participant = $participants[$next_participant_idx];

            $sql = "UPDATE `participante` SET id_participante_sorteado = " . $next_participant["id"] . " WHERE id_participante = " . $participant["id"];

            if ($connection->query($sql) == FALSE) {
                $ret = array(
                    "message" => "Error shuffling: " . $connection->error,
                );

                http_response_code(500);

                die(json_encode($ret));
            }
        }
    }

    function ShuffleArray(&$array) {
        $length = count($array);

        for($idx = 0; $idx < $length; $idx++) {
            $rand_idx = mt_rand(0, $length - 1);
            $tmp = $array[$idx];
            $array[$idx] = $array[$rand_idx];
            $array[$rand_idx] = $tmp; 
        }
    }
?>