<?php
  $id = $_GET["id"];

  $amigo_secreto = Load($id);

  print(json_encode($amigo_secreto));

  function Load($id) {
    include 'config.php';

    $connection = new mysqli($DB_SERVER, $DB_USER, $DB_PASS, $DB_NAME);

    if($connection->connect_error) {
      $ret = array(
        "message" => "Connection failed: " . $connection->connect_error,
      );

      http_response_code(500);

      die(json_encode($ret));
    }

    $ret = array();

    $ret["id"] = $id;
    $ret["name"] = LoadAmigoSecreto($connection, $id);
    $ret["participants"] = LoadParticipants($connection, $id);

    return $ret;
  }

  function LoadParticipants($connection, $id) {
    $sql = "SELECT id_participante, nm_participante, uuid FROM `participante` WHERE id_amigo_secreto = " . $id;
    $result = $connection->query($sql);

    if ($result == FALSE) {
      $ret = array(
        "message" => "Unable to load participants. " . $connection->error,
      );

      http_response_code(500);

      die(json_encode($ret));
    }

    if ($result->num_rows == 0) {
      $ret = array(
        "message" => "There are no participants in this amigo secreto :(. How Come?! " . $connection->error,
      );

      http_response_code(500);

      die(json_encode($ret));
    }

    $participants = array();

    while ($row = $result->fetch_row()) {
      $participant = array();

      $participant["id"] = $row[0];
      $participant["name"] = $row[1];
      $participant["uuid"] = $row[2];

      array_push($participants, $participant);
    }

    return $participants;
  }

  function LoadAmigoSecreto($connection, $id) {
    $sql = "SELECT nm_amigo_secreto FROM `amigo-secreto` WHERE id_amigo_secreto = " . $id;
    $result = $connection->query($sql);

    if ($result == FALSE) {
      $ret = array(
        "message" => "Unable to load Amigo Secreto. " . $connection->error,
      );

      http_response_code(500);

      die(json_encode($ret));
    }

    if ($result->num_rows == 0) {
      $ret = array(
        "message" => "Amigo Secreto has no data. " . $connection->error,
      );

      http_response_code(500);

      die(json_encode($ret));
    }

    $row = $result->fetch_row();

    if ($row == NULL) {
      $ret = array(
        "message" => "Unable to load data from Amigo Secreto. " . $connection->error,
      );

      http_response_code(500);

      die(json_encode($ret));
    }

    return $row[0];
  }
?>