<?php
  $id = $_GET["uuid"];

  $amigo_secreto = Load($id);

  print(json_encode($amigo_secreto));

  function Load($id) {
    include 'config.php';

    $connection = new mysqli($DB_SERVER, $DB_USER, $DB_PASS, $DB_NAME);

    if($connection->connect_error) {
      $ret = array(
        "message" => "Connection failed: " . $connection->connect_error,
      );

      http_response_code(500);

      die(json_encode($ret));
    }

    $ret = LoadFromDataBase($connection, $id);

    return $ret;
  }

  function LoadFromDataBase($connection, $id) {
    $ret = array();

    $ret["name"] = "";
    $ret["name_sorteado"] = "";

    $sql = "SELECT a.nm_participante, b.nm_participante FROM `participante` AS a JOIN `participante` AS b ON b.id_participante = a.id_participante_sorteado WHERE a.uuid = '" . $id . "'";

    $result = $connection->query($sql);

    if ($result == FALSE) {
      $ret = array(
        "message" => "Unable to load Amigo Secreto. " . $connection->error,
      );

      http_response_code(500);

      die(json_encode($ret));
    }

    if ($result->num_rows == 0) {
      $ret = array(
        "message" => "Amigo Secreto has no data. " . $connection->error,
      );

      http_response_code(500);

      die(json_encode($ret));
    }

    $row = $result->fetch_row();

    if ($row == NULL) {
      $ret = array(
        "message" => "Unable to load data from Amigo Secreto. " . $connection->error,
      );

      http_response_code(500);

      die(json_encode($ret));
    }

    $ret["name"] = $row[0];
    $ret["name_sorteado"] = $row[1];

    return $ret;
  }
?>